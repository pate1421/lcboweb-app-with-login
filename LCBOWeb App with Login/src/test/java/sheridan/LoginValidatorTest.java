package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testCheckIfValidLength( ) {
		assertTrue("Invalid login" , LoginValidator.checkIfValidLength( "eodfkr" ) );
	}
	
	@Test
	public void testNotStartWithNumber( ) {
		assertTrue("Invalid login" , LoginValidator.notStartWithNumber( "fer123" ) );
	}

}
