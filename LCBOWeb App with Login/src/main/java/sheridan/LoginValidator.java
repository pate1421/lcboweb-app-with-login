package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		return loginName.length( ) > 4 ;
	}
	
	//For a part where must have alpha characters and numbers only, but should not start with a number
	public static boolean notStartWithNumber(String userLoginName)
	{
		String regex = "^[A-Za-z]\\w*$";

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(userLoginName);
		
		return matcher.matches(); 

	}
	
	// This is for the part for having 6 alphanumeric characters
	public static boolean checkIfValidLength(String userLoginName) 
	{
		boolean totalCheckUp = false;
		int ct = 0;
		
		for(char u : userLoginName.toCharArray())
		{
			if(Character.isLetter(u)) 
			{
				ct++;
			}
			if(ct==6) 
			{
				totalCheckUp = true;
				break;
			}
		}
		return totalCheckUp;
	}
}
